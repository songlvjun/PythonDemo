import matplotlib.pyplot as plt

squares = [1, 13, 4, 6, 9, 11]

# linewidth标记了线的粗细
plt.plot(squares, linewidth=5)

# 设置图表标题
plt.title("2019 city rain", fontdict={"fontsize": 20, "color": "red"})
plt.xlabel("value", fontdict={"fontsize": 14, "color": "red"})
plt.ylabel("month of value", fontdict={"fontsize": 14, "color": "red"})

# 设置刻度标记大小
plt.tick_params(axis='both', labelsize=14)
plt.show()

# 传递一个x和y坐标
plt.scatter(2, 4, c='red', s=200)
plt.title("Square Numbers", fontsize=24)
plt.xlabel("Value", fontsize=14)
plt.ylabel("Square of Value", fontsize=14)

# 设置刻度标记的大小
plt.tick_params(axis='both', which='major', labelsize=14)
plt.show()


# 使用scatter() 绘制一系列的点
x_value = [1, 2, 3, 4, 5]
y_value = [1, 4, 9, 16, 25]

plt.scatter(x_value, y_value, s=100)
plt.show()

# 自动计算数据
# 创建x 0 ~ 1000值存储到列表中
x_values = list(range(1, 1001))
# 遍历x的值， 计算其平均值(x**2),并将结果存储都列表中
y_values = [x**2 for x in x_values]

# c自定义线的颜色
plt.scatter(x_values, y_values, c='red', edgecolors='none', s=40)
plt.axis([0, 1100, 0, 1100000])
plt.show()


# 采用RGB颜色定义
plt.title("RGB")
plt.scatter(1, 2, c=(0, 0, 6), s=40)
plt.show()


# 采用RGB映射
plt.title("RGB Mapping")
# 我们将参数c设置成一个y值列表，并使用参数cmap告诉pyplot使用哪种颜色进行映射
# 这些代码将y值小的点显示为浅蓝色, y值大的显示成深蓝色
plt.scatter(x_values, y_values, c=y_values, cmap=plt.cm.Blues, s=40)
plt.show()


# 自动保存图表
# 第一以什么名称保存图表, 第二将空白的部分裁剪到
'''
  x_value = [1, 2, 3, 4, 5]
  y_value = [1, 4, 9, 16, 25]
  plt.scatter(x_value, y_value, c=y_value, cmap=plt.cm.Blues, s=40)
  plt.savefig('squares_plot.png', bbox_inches='tight')
'''
