# pizza module use to show the list
def make_pizza(size, *toppings):
    print("\n Making a " + str(size) +
          "-inch pizza with following toppings")
    for topping in toppings:
        print("- " + topping)


def show_hello():
    return 'welcome to use this module'
    

def say_goodbye():
    print('good bye boy')


def show_names(name_list):
    for name in name_list:
        print(name)

