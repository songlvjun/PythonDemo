# 在本模块中如依赖另一个模块的类，可以将其导入。
import pizza as p

# 在一个模块中存储多个类
class Person():

    def __init__(self, name, age, address):
        self.name = name
        self.age = age
        self.address = address

    def get_desc_info(self):
        info = self.name + " " + str(self.age) + " " + self.address
        return info.title()

    def get_age(self):
        return self.name + " age is : " + str(self.age) + " years old "

    def set_age(self, age):
        self.age = age

    def get_address(self):
        return self.name + " 现居住地: " + self.address


# 定义单独的花朵类
class Flower():

    def __init__(self, flower_name='水仙花', flower_color='红色', flower_size=40):
        self.flower_name = flower_name
        self.flower_color = flower_color
        self.flower_size = flower_size

    def get_flower_info(self):
        return "花朵名字: " + self.flower_name + ", 花朵颜色: " + self.flower_color + ", 花朵尺寸: " + str(self.flower_size)

    def get_flower_name(self):
        return self.flower_name

    def show_hello(self):
        return self.flower_name + " " + p.show_hello()


