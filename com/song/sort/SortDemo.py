"""
  sorted(iterable[,key][,reverse])返回一个排序后的新序列，不改变原始的序列。
  第一个参数: iterable, 可迭代的对象
  第二个参数key： 用来指定一个带参数的函数，key指定的函数将作用于list的每一个元素上，并根据key指定的函数返回的结果进行排序。
  第三个参数reverse：用来指定正向还是反向排序

"""


# 定义排序方法
def sortNum():
    array = [1, 9, 4, 2, 10]
    print(array)
    # 反转结果 [10, 9, 4, 2, 1]
    newArray = sorted(array, reverse=True)
    print(newArray)

def sortDict():
    array = [
        {"id": 1, "name": "a90", "age": 40},
        {"id": 2, "name": "b", "age": 10},
        {"id": 3, "name": "a63", "age": 30},
        {"id": 4, "name": "a62", "age": 30},
    ]
    # 按照年龄正序
    res = sorted(array, key=lambda x: (x['age']))
    print(res)
    # 按照年龄、name 多个字段排序
    res = sorted(array, key=lambda x: (x['age'], x['name']))
    print(res)

if __name__ == '__main__':
    # sortNum()
    sortDict()
