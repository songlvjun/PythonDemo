# 从模块中引入指定的函数,
# 如果模块的函数名与当前文件的函数发生冲突，可以给导入模块的函数起别名

from pizza import show_hello as hello, say_goodbye, make_pizza
# 导入整个模块
import sys as system  # 使用as给模块指定别名

make_pizza(16, 'chocolate')
make_pizza(20, 'green pepper', 'extra cheese')

# 使用as给函数指定别名
hello()
say_goodbye()
print('python的路径为: ', system.path)

'''
  导入模块中的所有函数
    from pizza import *

  import pizza 
    这样做并没有把直接定义的pizza中的函数名称写入当前的符号表里,只是把模块pizza的名称
    写在了那里。
  具体使用时:
    如 pizza.say_hello() 直接使用模块名来访问函数。
    
'''


'''
  函数编写指南:
     编写函数时, 需要牢记几个细节。应该给函数指定描述性名称，且只在其中使用 小写字母和下划线。
     每个函数都应该包含简要的功能注释，该注释应该跟在函数定义的后面
'''


