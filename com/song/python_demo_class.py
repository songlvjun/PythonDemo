# study python class 2019-02-13


# 创建Dog类, 每个实例都将存储名称和年龄，并赋予其 sit() 蹲下 和 roll_over() 打滚的能力
class Dog():

    def __init__(self, name, age):
        '''初始化属性name和age'''
        self.name = name
        self.age = age

    def sit(self):
        '''模拟蹲下的动作'''
        print(self.name.title() + " now is sitting")

    def roll_over(self):
        '''模拟打滚动作'''
        print(self.name.title() + " now is roll_over")


'''
    _init_() 方法 是一个特殊的方法。
        每当根据Dog类创建新实例时，python都会自动运行它。
    在这个方法的名称中，开头和末尾都各自有两个下划线。
        这是一种约定，旨在避免Python默认方法与普通方法发生名称冲突
    
    _init_(self, name, age) 在这个方法中 self 必不可少，而且还必须位于其他形参前面
    为什么要定义self呢？ 因为Python 在调用_init_()方法创建Dog的实例时，将自动传递self,
    self是指向实例本身的引用,让实例能够访问到类中的属性和方法
    
    
'''

# 创建Dog类的实例
dog = Dog('lucy', 20)
dog.roll_over()
dog.sit()

# 访问类的属性
print("my dog name is " + dog.name)
# 注意在 打印int值时 需要将其转成str
print("my dog age is " + str(dog.age))

# 创建多个实例
dog_1 = Dog('cary', 30)
dog_2 = Dog('mata', 30)

print(dog_1.name + " : " + str(dog_1.age))
print(dog_2.name + " : " + str(dog_2.age))


'''
    使用类和实例, 你需要执行修改实例的属性.
        你可以直接修改实例的属性,也可以编写方法修改实例的属性
        
    Car 类
'''


class Car():

    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        # 给属性设置默认值
        self.odometer_reading = 0

    def get_desc_info(self):
        info = self.make + " " + self.model + " " + str(self.year)
        return info.title()

    def get_odometer(self):
        return self.make + " is use : " + str(self.odometer_reading)

    # 更新里程数
    def update_odometer(self, mileage):
        # 可以禁止将里程数往回调
        if mileage >= self.odometer_reading:
            self.odometer_reading = mileage
        else :
            print("you cannot roll back an odometer!")

    # 更新里程数, 以递增形式
    def increment_odometer(self, mileage):
        self.odometer_reading += mileage


my_car = Car('audi', 'a4', 2019)
print(my_car.get_desc_info())
print("================打印属性值====================")
print(my_car.get_odometer())

'''
    修改属性值
        一共有三种方法:
        1. 直接修改属性值
        2. 通过方法修改属性值
'''

print("================直接修改属性值====================")
my_car.odometer_reading = 20
print(my_car.get_odometer())

print("================通过方法修改属性值====================")
my_car.update_odometer(30)
print(my_car.get_odometer())

print("================禁止将数值往回调====================")
my_car.update_odometer(10)

print("================以递增的形式====================")
my_car.increment_odometer(20)
print(my_car.get_odometer())
my_car.increment_odometer(20)
print(my_car.get_odometer())

