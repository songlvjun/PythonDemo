# study python 2019-02-12


# 定义简单函数
def greet_user():
    print("Hello world")


greet_user()


# 向函数传递值
def greet_username(show_name):
    print(show_name)


greet_username("my name is jack")


# 传递实参
def describe_pet(animal_type, pet_name):
    print(" I have a animal is belong of " + animal_type +
          " and his name is " + pet_name)


# 使用关键字实参时, 务必准确第指出函数定义中的形参名
describe_pet("tiger", "jacky")
# 关键字传参
describe_pet(animal_type="mouse", pet_name="jack")
# 关键字传参
describe_pet(pet_name="caty", animal_type="dog")


# 默认值 编写函数时，可以给每个形参指定默认值
def describe_pet(animal_type, pet_name='dog'):
    print(" I have a animal is belong of " + animal_type +
          " and his name is " + pet_name)


describe_pet("cuckold")
describe_pet(animal_type="cuckold")

# 避免实参错误
# TypeError: describe_pet() missing 1 required positional argument: 'animal_type'
# describe_pet()


# 返回值
def get_formatted_name(first_name, last_name):  # 返回简单值
    full_name = first_name + ' ' + last_name
    return full_name


name = get_formatted_name("song", "lvjun")
print("===============返回值=====================")
print(name)


# 让实参变成可选的
def get_formatted_name(first_name, middle_name, last_name):
    full_name = first_name + ' ' + middle_name + ' ' + last_name
    return full_name.title()


name = get_formatted_name('song', 'lv', 'jun')
print(name)


# 然而并非是所有人都是有中间名字的
def get_formatted_name(first_name, last_name, middle_name=''):
    if middle_name:
        full_name = first_name + ' ' + middle_name + ' ' + last_name
    else:
        full_name = first_name + ' ' + last_name
    return full_name.title()


name = get_formatted_name('song', 'lv', 'jun')
print(name)

print("如果实参不存在,则不显示")
name = get_formatted_name('song', 'jun')
print(name)


# 返回字典
def build_person(first_name, last_name):
    person = {'first': first_name, 'last': last_name}
    return person


print("===============返回字典======================")
name = build_person('song', 'lv jun')
print(name)


# 传递列表
def greet_users(names_list):
    for user_name in names_list:
        print("Hello " + user_name + " welcome join us ")


print("===============传递列表======================")
names = ['songlvjun', 'zhangsan', 'lisi']
greet_users(names)


#  在函数中修改列表
unprinted_list = ['iphone', 'xiaomi', 'huawei', 'vivo']
complete_list = []

print("===============原始列表======================")
print(unprinted_list)
print(complete_list)

# 模拟打印 直至结束为止
while unprinted_list:
     complete_list.append(unprinted_list.pop())

# 模拟打印 使用for循环遍历可以，但是在循环过程中删除集合元素可能会导致一些问题
# for value in unprinted_list:
#     complete_list.append(unprinted_list.pop())


print("===============操作之后列表======================")
print(unprinted_list)
print(complete_list)

# 列表遍历
cars = ['bike', 'car', 'bmw', 'train']


def greet_cars(cars_list):
    if cars_list:
        for v in cars_list:
            print(v)


print("===============遍历列表======================")
greet_cars(cars)
print("===============遍历列表======================")
unprinted_list = ['iphone', 'xiaomi', 'huawei', 'vivo']
greet_cars(unprinted_list)


# 禁止函数修改列表 可以向函数传递 list副本，而不是直接的list
def show_list(name_list):
    while len(name_list) > 0:
        value = name_list.pop()
        print(value)


name_list = ['john', 'lucy', 'carder', 'lemon']
print("===============列表原先内容======================")
print(name_list)
print("===============禁止函数修改列表, 传递副本======================")
show_list(name_list[:])
print("==============列表内容======================")
print(name_list)

'''
   虽然向函数传递列表的副本可以保留原始函数列表，但除非有充分的理由需要传递副本，否则还是应该将原始列表传递给函数
   因为使用现有列表可以避免花时间和内存创建副本
'''


# 传递任意参数的实参
def make_pizza(*toppings):
    print(toppings)


print("==============传递任意实参======================")
make_pizza('carrot')
make_pizza('cabbage', 'greens')
make_pizza('tomato', 'chilli', 'honey peach')


def make_pizza(*toppings):
    for topping in toppings:
        print("- " + topping)


print("==============遍历任意实参======================")
make_pizza('carrot')
print()
make_pizza('cabbage', 'greens')
print()
make_pizza('tomato', 'chilli', 'honey peach')


'''
  使用任意数量的关键字实参, 有时我们不知道传递给函数的是什么信息。但在这种情况下，我们可将函数编写成能够接受任意数量的键值对
'''


def build_profie(first_name, last_name, **user_info):
    profile = {}
    profile['first'] = first_name
    profile['last'] = last_name
    for k, v in user_info.items():
        profile[k] = v
    return profile


print("==============遍历任意实参======================")
info_dict = build_profie("song", "lv", age=20,address='shanghai')
print(info_dict)

''' 
    将函数存储在模块之中. 要让函数是可导入的。首先的创建模块，模块是扩展名为.py的文件，包含要导入到程序中的代码
    下面创建一个包含函数 make_pizza()的模块
'''

