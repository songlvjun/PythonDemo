# study python 2019-02-12

# 用户输入 input()与while循环
name = input("please enter your name!")
print("welcome to you : " + name)

# 指定输入的类型是int
age = input("please enter your age!")
print("your age is : " + age)

number = input("please input your num, we can judge it is odd or even")
# 用户输入接收的是字符串，需要转为int
if number:
    number = int(number)
    if number % 2 == 0:
        print(str(number) + " is even")
    else:
        print(str(number) + " is odd")


# while 循环
count = 1
while count < 15:
    print(count)
    count += 1


# 给定一个数组 分别取出奇数集 和 偶数集
numbers = [12, 17, 4, 2, 10, 20]
odd = []
even = []

# 可以使用for循环变量判断奇偶数，增加到对应数组中
# while循环 只有一个个减少 直至为空

while len(numbers) > 0:
    number = numbers.pop()
    if number % 2 == 0:
        even.append(number)
    else:
        odd.append(number)
print(odd)
print(even)

# 让用户决定何时退出

prompt = "tell me something, and i will repeat it back to you, enter quit can exit"
message = ""
# while message != 'quit':
#      message = input(prompt)
#      print(message)

# 在列表之间移动元素

# 未验证的用户列表
unconfirmed_users = ['alice', 'brain', 'candace']
# 已验证的用户列表
confirmed_user = []

# 验证每个用户，直至验证完成
while unconfirmed_users:
    user = unconfirmed_users.pop()
    confirmed_user.append(user)

print("====================所有已验证列表======================")
# 所有已验证列表
for user in confirmed_user:
    print(user)

# 删除包含特定值的所有列表元素 remove可以删除列表中指定值，但如果多个的话，可以循环删除
pets = ['cat', 'dog', 'tiger', 'cat', 'pig']


print("====================删除之前======================")
print(pets)
while 'cat' in pets:
    pets.remove('cat')

print("====================删除包含特定值的所有列表元素======================")
print(pets)

# 将input输入的值存储到字典之中

nameArray = {}
poll_active = True


print("====================将input输入的值存储到字典之中======================")
while poll_active:
    # 提示输入被调查者的名字和回答
    name = input("what is your name ?")
    love = input("which cities do you like")
    nameArray[name] = love

    # 提示是否有人需要提问
    repeat = input('Are there other people who want to ask questions? please input yes or no')
    if repeat == 'no':
        poll_active = False

print(nameArray)