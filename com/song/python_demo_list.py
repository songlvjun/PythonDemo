
# study python 2019-02-11

fruits = ['math','english','java']

# 新增元素
fruits.append("123")
print(fruits)

# 在指定的位置，新增元素
fruits.insert(0,"song")
print(fruits)

# 删除元素
del fruits[0]
print(fruits)

# 删除元素
fruits.pop()
print(fruits)

# 弹出指定位置的元素
english = fruits.pop(1)
print(english)
print(fruits)

fruits = ['math','english','java']
print(fruits)

deleteKey = "math"
# 根据值来删除元素
fruits.remove(deleteKey)

print("a math is too expensive for me")
print(fruits)

fruits = ['math','english','java']
# 对元素进行排序
print(fruits)
fruits.sort()
print(fruits)

# fruits.sort(reversed=True)

print(fruits)
# 倒着打印列表, 反转元素列表顺序
fruits.reverse()
print(fruits)

# 列表长度
print(len(fruits))

# 遍历列表
for k in fruits:
    print(k.title())

for i in range(0,10):
    print(i)

# 生成 5 个值
for i in range(5):
    print(i)

# 创建数字列表 使用 list() 将 range()的数字结果直接转换为列表
numbers = list(range(0,10))
print(numbers)


# 使用 range() 还可以指定步长
numbers = list(range(0,100,10))
print(numbers)

# 对数字列表进行简单的统计计算
digits = [1,2,3,4,5,6,7,8,9]
# 求最小
print(min(digits))
# 求最大
print(max(digits))
# 求和
print(sum(digits))


# 列表解析

# 创建平方数列表
squares = [value**2 for value in range(1,11)]
print(squares)

# 使用列表的一部分, 成为切片
player = ['song','lv','jun','is','a','boy']

# 打印列表全部元素
print(player[:])

# [0,4) 不包含 4 输出前4位
print(player[0:4])

# 如果没有指定开始 则默认列表开头为开始
print(player[:3])

# 返回第2个元素一直到最后
print(player[2:])

# 返回后三名元素
print(player[-3:])

# 遍历切片 指定为列表的前4个元素
for k in player[0:4]:
    print(k.title())


# 复制列表
my_foods = ['pizza','fared','carry']
# 从my_foods提取了一个切片,赋值给了 friend_foods
friend_foods = my_foods[:]
print(friend_foods)

my_foods.append("apple")
# ['pizza', 'fared', 'carry', 'apple']
print(my_foods)
# 此时浅复制的列表, 并没有apple元素
print(friend_foods)

# 两个对象都指向了同一个列表,所以打印结果一样
foods = my_foods
foods.append("level")
print(foods)
print(my_foods)

'''
 列表非常适合存储在程序运行期间可能变化的数据集。列表是可以修改的.
 
 python 将不能修改的值成为 不可变的,称为元组
 元组看起来是列表，但使用圆括号而不是方括号来标识，定义元组后，可以使用索引来访问其元素
'''

dimensions = (200,50)
print(dimensions[0])
print(dimensions[1])

# 试图修改元组数值,不允许
# dimensions[0] = 100 print(dimensions[0])

# 遍历元组所有元素
for value in dimensions:
    print(value)

# 虽然不能修改元组的元素，但可以给存储元组的变量重新赋值, 可以重新定义整个元组
dimensions = (200,40)

for value in dimensions:
    print(value)










