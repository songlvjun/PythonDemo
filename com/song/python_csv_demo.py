# read csv info
# 导入csv模块
import csv
from matplotlib import pyplot as plt
from datetime import datetime

filename = r'file\sitka_weather_07-2014.csv'
highs = []
dates = []
with open(filename) as f:
    '''
      打开指定文件, 将结果文件对象存储在 f, 调用csv.reader().
      reader处理文件中以逗号分隔的第一行数据, 并将每项数据都作为一个元素存储到列表中
    '''
    reader = csv.reader(f)
    # 返回文件中的下一行, 因为我们只调用了一次,所以只会读取第一行
    header_row = next(reader)
    '''
      为了使文件头数据更容易理解,将列表中的每个文件头以及其位置都打印出来
      enumerate(),获得其索引和值
      for index, column_header in enumerate(header_row):
         print(index, column_header)
    '''
    # 读取文件中最高气温
    for row in reader:
        # 每一行, 第一个元素存储的是时间, 第二个元素存储的是最高气温
        high = int(row[1])
        highs.append(high)

        current_date = datetime.strptime(row[0], '%Y-%m-%d')
        dates.append(current_date)

# 根据数据绘制图形
fig = plt.figure(dpi=128, figsize=(10,6))
# 折线图
plt.plot(dates,highs,c='red')
# 标题
plt.title("Daily high temperatures july 2014", fontsize=24)
# x轴标题
plt.xlabel('',fontsize=14)
# Y轴标题
plt.ylabel('Temperatures (F)', fontsize=14)
# 设置x轴日期是倾斜的，避免重合
fig.autofmt_xdate()
plt.tick_params(axis='both', which='major', labelsize=16)


filename = r"file\sitka_weather_2014.csv"
death_filename = r'file\death_valley_2014.csv'
with open(death_filename) as f:

    reader = csv.reader(f)
    # 读取下一行
    header_row = next(reader)

    highs, dates, lows = [], [], []

    for row in reader:
        try:
            date = datetime.strptime(row[0], '%Y-%m-%d')
            high = int(row[1])
            low = int(row[2])
            dates.append(date)
            # 最高气温
            highs.append(high)
            # 最低气温
            lows.append(low)
        except ValueError:
            print(date, 'missing data')

    fig = plt.figure(dpi=128, figsize=(10,6))

    plt.title("Daily high temperatures - 2014", fontsize=16)
    # 绘制x轴坐标倾斜
    plt.xlabel('', fontsize=14)
    fig.autofmt_xdate()
    plt.ylabel('high of temperatures', fontsize=14)

    plt.tick_params(axis='both', which='major', labelsize=16)

    # 最高气温的线
    plt.plot(dates, highs, c='red')
    # 最低气温的线
    plt.plot(dates, lows,  c='blue')

    # 绘制图表着色区域
    plt.fill_between(dates, lows, highs, facecolor='blue', alpha=0.1)

    # plt.savefig('Daily of city temperature')

    plt.show()