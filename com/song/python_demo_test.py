import unittest


def get_formatted_name(first, last):
    full_name = first + " " + last
    return full_name.title()


# print("Enter simple name, and you can also input 'q' to quit")
# while True:
#     first = input("input your first name?")
#     if first == 'q':
#         break
#     last = input("input your last name?")
#     if last == 'q':
#         break
#     username = get_formatted_name(first, last)
#     print(username)


class NamesTestCase(unittest.TestCase):

    '''
       首先, 我们导入了模块unittest和要测试的函数get_formatted_name(), 测试必须要继承 unittest.TestCase类
    '''
    def test_first_last_name(self):
        formatted_name = get_formatted_name('song', 'lvjun')
        '''
            断言方法，判断得到的值是否与期望值一致
        '''
        #self.assertEquals(formatted_name, 'Song LvJun')
        self.assertNotEqual(formatted_name, 'Song LvJun')
#unittest.main()


class AnnoymousSurver():

    def __init__(self, question):
        self.question = question
        self.responses = []

    def show_question(self):
        print(self.question)

    def store_response(self, response):
        self.responses.append(response)

    def show_result(self):
        print("Surver result : ")
        for result in self.responses:
            print("_" + result)


# question = 'what language did first learn to speak'
# my_annmous = AnnoymousSurver(question)
#
# while True:
#     response = input("language: ")
#     my_annmous.store_response(response)
#     if response == 'q':
#         break
# my_annmous.show_result()


class TestAnnomousSurver(unittest.TestCase):

    # 定义一个方法进行验证。如果用户面对调查问卷时只提供了一个答案，这个答案也能妥善地存储，我们将在这个答案被存储后。
    # 使用方法assertIn() 来核实是否包含在答案列表中
    def test_store_single_response(self):
        question = 'what language did first learn to speak'
        my_annmous = AnnoymousSurver(question)
        my_annmous.store_response('java')
        self.assertIn('java', my_annmous.responses)


    '''
        在之前的测试中，我们在每次测试实例都创建了AnnomousSurver实例，
        unittest.TestCase类包含setUp()，让我们只需要创建这些对象一次，并在每个测试实例中都可以使用它，如果你在
        TestCase()中包含了setUp()方法，Python将先运行它，再运行各自以test_打头的方法
    '''


class TestAnnomouseArray(unittest.TestCase):

    def setUp(self):
        question = 'what language did first learn to speak'
        # 定义一个问题
        self.my_annmous = AnnoymousSurver(question)
        # 定义一组问卷答案
        self.responses = ['java', 'javascript', 'eclipse']

    def test_store_single_response(self):
        # 调用类存储一个答案
        self.my_annmous.store_response(self.responses[0])
        # 断言
        self.assertIn( self.responses[0],self.my_annmous.responses)

    def test_store_three_response(self):
        # 存储一组答案
        for response in self.responses:
            self.my_annmous.store_response(response)
        # 断言
        for response in self.responses:
            print(self.responses)
            self.assertIn(response, self.my_annmous.responses)
            print(self.my_annmous.responses)

unittest.main()
