# study python class 2019-02-15
# 导入一个模块的多个类
from person import Person, Flower
# 导入一个模块的所有类
# from person import *
'''
    导入类
    随着你给类不断的添加能，文件可能会变得很长。
    python允许你将类存储在模块中，然后主程序导入所需的模块
'''

ning = Person("小明", 20, '居住在上海')
print(ning.get_desc_info())
print(ning.get_age())

'''
   定义子类
   1. 创建子类时, 父类必须包含在当前文件中, 且位于子类前面
   2. 创建子类时, 括号内指定父类的名称。方法__init__() 接收创建Car实例所需信息
   
'''

print("===================将实例作为属性============================")
'''
  将实例用做属性
    使用代码模拟实物时，你会发现给自己的类添加了越来越多的细节，属性和方法的清单都很长,
    这个时候你需要将其中一部分提取出来作为 单独的类。
        例如本例中，my_ming 喜欢的花朵，可以将花朵单独提取出来，花朵的颜色、尺寸、品类
'''

class XiaoMing(Person):

    def __init__(self, name, age, address):
        super(XiaoMing, self).__init__(name, age, address)
        # 独特之处，初始完父类属性，定义自己独有的子类属性
        self.favorite = Flower()

    def get_favorite_info(self):
        return self.name + " is love " + self.favorite.flower_name

    def get_address(self):
        return "now subclass address is : " + self.address


my_ming = XiaoMing('zhangsan', 30, '现居住在宿迁')
print(my_ming.get_desc_info())
print(my_ming.get_age())
print(my_ming.get_address())

print("===================定义子类独有属性和方法============================")
print(my_ming.favorite.flower_name)
print(my_ming.get_favorite_info())

print("===================重写父类方法============================")
print(my_ming.get_address())

print("===================模块导入多个类============================")
print(my_ming.favorite.get_flower_info())

print("===================模块导入另一个模块所需函数或者类============================")
print(my_ming.favorite.show_hello())

'''
    python 标准库是一组模块，安装的Python都包含它，你可以使用其中任意的函数和类，只需要import
'''
from collections import OrderedDict

#  OrderedDict 有序字典 兼具列表和字典的主要优点(将信息关联起来，同时保留原来的顺序)

favorite_game = OrderedDict()
favorite_game['song'] = "crossfire"
favorite_game['xiaoming'] = "crossfire"
favorite_game['lilei'] = "lol"
favorite_game['jack'] = "naruto"

print("===================引入python标准库============================")
for name, game_name in favorite_game.items():
    print(name + " love " + game_name)

