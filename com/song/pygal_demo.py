# use pygal
from random import randint
import pygal


class Die():
    '''表示一个骰子类'''
    def __init__(self, num_sides=6):
        '''默认骰子为6面'''
        self.num_sides = num_sides

    def roll(self):
        '''返回一个位于1和骰子面数之间的数字'''
        return randint(1, self.num_sides)


# x_labels 可能结果作为x轴
# frequencies 分析结果
# file_name 存储svg 图表名称
def store_svg(x_labels, frequencies, file_name):
    # 对数据进行可视化
    hist = pygal.Bar()

    hist.title = "Results of rolling one D6 1000 times"
    hist.x_labels = x_labels
    hist.x_title = "Result"
    hist.y_title = "Frequency of Result"

    hist.add('D6', frequencies)
    hist.render_to_file(file_name)


# 创建一个D6
die = Die()
results = []
for num in range(1000):
    result = die.roll()
    results.append(result)

print(results)

# 分析结果
frequencies = []
for value in range(1, die.num_sides + 1):
    # 每个数字出现的次数
    frequency = results.count(value)
    frequencies.append(frequency)
print(frequencies)

svg_name = 'die_visual.svg'
x_labels = [1, 2, 3, 4, 5, 6]
store_svg(x_labels, frequencies, svg_name)

# 同时扔两个骰子
die_1 = Die()
die_2 = Die()

results = []
for value in range(1000):
    result = die_1.roll() + die_2.roll()
    results.append(result)

# 计算出现的和
frequencies = []
max_result = die_1.num_sides + die_2.num_sides
for value in range(1, max_result + 1):
    frequency = results.count(value)
    frequencies.append(frequency)

svg_name = 'die_two_visual.svg'
x_labels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
store_svg(x_labels, frequencies, svg_name)




