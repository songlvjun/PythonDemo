# 随机漫步, 模拟蚂蚁在晕头的情况下，每一次都沿随机方向行走所经过的路径
from random import choice
from matplotlib import pyplot as plt


class RandomWalk():

    def __init__(self, num_points=5000):
        self.num_points = num_points

        # 所有随机漫步都始于(0,0)
        self.x_values = [0]
        self.y_values = [0]

    def fill_walk(self):
        '''不断的漫步,直达到达指定的长度'''
        while len(self.x_values) < self.num_points:
            # 决定前进方向以及沿这个方向前进多少距离

            # 给定 x 方向 是 向左还是向右
            x_direction = choice([1, -1])
            # 随机走的值
            x_distance = choice([0, 1, 2, 3, 4])
            # 如果为正 将向右移动，为负则向左移动
            x_step = x_direction * x_distance

            y_direction = choice([1, -1])
            y_distance = choice([0, 1, 2, 3, 4])
            y_step = y_direction * y_distance

            # 拒绝原地踏步
            if x_step == 0 and y_step == 0:
                continue

            # 计算下一个点的 x 和 y 值
            next_x = self.x_values[-1] + x_step
            next_y = self.y_values[-1] + y_step

            self.x_values.append(next_x)
            self.y_values.append(next_y)


# 只要程序运行就可以不断的模拟
while True:
    rw = RandomWalk(50000)
    rw.fill_walk()

    point_numbers = list(range(rw.num_points))

    plt.scatter(rw.x_values, rw.y_values, c=point_numbers,
                cmap=plt.cm.Blues, edgecolors='none', s=15)

    # 突出起点和终点
    plt.scatter(0, 0, c='green', edgecolors='none', s=100)
    plt.scatter(rw.x_values[-1], rw.y_values[-1], c='red', edgecolors='none', s=100)

    # 隐藏坐标轴
    plt.axis('off')
    # 调整绘制窗口的尺寸
    # figure指定图表的宽度、高度、分辨率和背景色
    plt.figure(dpi=128, figsize=(16, 9), facecolor="none")
    plt.show()

    keep_running = input("Make another walk? (y/n)")
    if keep_running == 'n':
        break



