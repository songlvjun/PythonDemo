# study python 2019-02-11

cars = ['audi','bmw','toyota','subaru']

for car in cars:
    if car == 'bmw':
        print(car.upper())
    else:
        print(car.lower())

car = "Audi"
# python 咋检查是否相等时不考虑大小写
print(car == 'audi')  # False
print(car == 'AUDI')  # False
print(car.lower() == 'audi')  # True

# 检查是否不相等
age = 20
if age != 10:
    print("yes")

# 检查特定的值是否在列表中存在
encryptionMethod = ['hash','md5','aes']
if 'hash' in encryptionMethod:
    print("the key is exist in list")
else:
    print("the key is not exist in list")

# 检查特定的值是否不存在列表中
if 'song' not in encryptionMethod:
    print("yes")
else:
    print("no it exist")

# if else 语句
num = 20
if num > 10:
    print("the result is true")
else:
    print("the result is false")

# if-elif-else 语句
age = 20
if age < 4:
    print("your are free")
elif age < 10:
    print("you need pay 4.0$")
else:
    print("you need pay 14.0$")

# 确定列表不是空的

# python 在列表至少有一个元素时返回 True. 空列表返回 False
array = ['1']
if array:
    print("ok finish your pizza")
else:
    print("Are you sure you want a plain pizza")
