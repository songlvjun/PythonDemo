# study python 2019-02-15
import json
"""
   Python使用被称为异常的特殊对象来管理程序执行期间发生的错误。
   每当让Python不知所措时，它都会创建一个异常对象。如果你编写了处理异常的代码，那么程序将继续执行，
   如果你未对异常做处理，程序将停止, 并显示一个traceback.
   异常使用 try-except来处理
"""
print("===============模拟异常=================")
# print(5/0)

print("===============处理异常,给出友好提示=================")
try:
    print(5/0)
except:
    print("ZeroDivisionError: division by zero, 分母不能为 0")

print("===============else块=================")
print("give me two numbers, and i will divide them")
print("Enter 'q' to quit.")

# while True:
#     first_num = input("please input first number")
#     if first_num == 'q':
#         break
#     second_num = input("please input second number")
#     if second_num == 'q':
#         break
#     try:
#        answer = int(first_num) / int(second_num)
#     except:
#        print("ZeroDivisionError: division by zero, 分母不能为 0")
#     else:
#         print(answer)

# 处理文件异常
filename = "show.txt"
try:
    with open(filename) as file_object:
        contents = file_object.read()
        print(contents)
except FileNotFoundError:
    msg = "Sorry, the file" + filename + " does not exist"
    print(msg)

print("===============计算文件内的单词个数=================")


def count_words(filename):
    try:
        with open(filename, encoding='utf-8') as file_object:
            contents = file_object.read()
    except FileNotFoundError:
        msg = "Sorry, the file" + filename + " does not exist"
        print(msg)
    else:
        words = contents.split()
        nums = len(words)
        print("The file " + filename + " has about " + str(nums))


filename = r"file\number.txt"
count_words(filename)

print("===============遍历文件,计算单词个数=================")
filenames = [r'file\abc.txt',r'file\song.txt',r'file\number.txt',r'file\test.txt']
for filename in filenames:
    count_words(filename)

print("===============异常时,程序一声不吭=================")
try:
    print(5/0)
except:
    pass  # pass可在代码块中使用, 它可以让python什么都不做处理。不会显示traceback,也没有任何的输出

# 存储数据  JSON格式
print("===============使用json,将数字列表存储到data.json中=================")
numbers = [1, 2, 3, 4, 5]
filename = r'file\data.json'
with open(filename, 'w') as file_object:
    json.dump(numbers, file_object) # json.dump将数字列表存储到了指定的文件对象之中
    print("存储成功")

print("===============使用json,读取data.json中=================")
with open(filename) as file_object:
    numbers = json.load(file_object)
    print(numbers)


username = input("what is your name?")
with open(filename, 'w', encoding='utf-8') as file_object:
    json.dump(username, file_object)

with open(filename, encoding='utf-8') as file_object:
    username = json.load(file_object)
    print(" hello " + username + ' i have accept your message')

print("===============如果以前存在就加载它=================")

filename = r'file\uername.json'
try:
    with open(filename) as file_object:
        username = json.load(file_object)
except FileNotFoundError:
    username = input("what is your name?")
    with open(filename, 'w', encoding='utf-8') as file_object:
        json.dump(username,file_object)
        print('welcome back ' + username)
else:
    print('welcome back ' + username)

print("===============定义问候函数=================")


def get_stored_name():
    try:
        with open(filename) as file_object:
            username = json.load(file_object)
    except FileNotFoundError:
        return None
    else:
        return username


print("===============封装函数=================")


def get_new_username():
    username = input("what is your name?")
    with open(filename, 'w', encoding='utf-8') as file_object:
        json.dump(username, file_object)
    return username


username = get_stored_name()
if username:
    print("welcome to you " + username)
else:
    username = get_new_username()
    print("welcome to you new " + username)
