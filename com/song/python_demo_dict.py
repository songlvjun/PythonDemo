# study python 2019-02-11

# 使用字典  在python中 字典是一系列键值对，每个键都与一个值相互关联

# 存储一些外星人 颜色和点数
alien_0 = {'color':'red','points':5}
print(alien_0['color'])

# 访问字典里面的值
print(alien_0['points'])

# 往字典里面添加值
alien_0["mode"] = 'primary'
print(alien_0)

# 创建一个空字典
alien_1 = {}

# 字典增加值
alien_1['color'] = 'pink'
alien_1['point'] = 20
alien_1['mode'] = 'nightmare'

print(alien_1)

# 修改字典里面的值
alien_1['point'] = 100
print(alien_1)

# 删除字典里面的值, 删除时就永远消失了
del alien_1['point']
print(alien_1)

# 由类似对象组成的字典
favorite_languages = {
    'song' : 'python',
    'sarah' : 'java',
    'phil' : 'javascript'
}

favorite_languages['phil'] = 'java'
print(favorite_languages)

# 遍历字典
user_0 = {
    'username' : 'zhangsan',
    'age' : '20',
    'address' : '上海'
}

print("遍历所有键值对")
for key, value in user_0.items():
    print('key: ' + key)
    print('value: ' + value)

print("============遍历所有键值对=======================")
for k, v in user_0.items():
    print('key: ' + k)
    print('value: ' + v)

print("=============遍历字典中所有的键======================")
# 遍历字典中所有的键

friends = ['username']
for name in user_0.keys():
    if name in friends:
        print(" Hi " + name.title())
    else:
        print("not exist")

print("===================================")

friends = ['username']
for name in user_0.keys():
    if name not in friends:
        print(" Hi " + name.title())
    else:
        print("you are exist: " + name)

print("============按顺序遍历所有的键=======================")
# 按顺序遍历所有的键
for name in sorted(user_0.keys()):
    print('name: ' + name)


print("===================================")
# 按顺序遍历所有的值
for value in sorted(user_0.values()):
    print('value: ' + value)

print("===============可以使用set集合====================")
# 提取字典里面的值 可能包含重复项，为了剔除重复项，可以使用set集合，使得每个元素都是独一无二的
languages = {
    'tim': 'java',
    'jack': 'python',
    'json': 'javascript',
    'lucy': 'java',
    'tom': 'c',
}

for value in set(languages.values()):
    print('value: ' + value)
print("==============遍历列表=====================")

# 嵌套

'''
  有时候需要将一系列字典存储在列表中，或者将列表作为值存储在字典中，这称为嵌套
'''

alien_0 = {'color':'pink','point':5}
alien_1 = {'color':'green','point':15}
alien_2 = {'color':'red','point':10}

# 将三个字典放入一个列表中
aliens = [alien_0,alien_1,alien_2]

# 遍历列表
for alien in aliens:
    print(alien)
print("===================================")

# 创建一个空的列表
aliens = []

for alien in range(10):
    alien = {'color':'pink','point':5}
    aliens.append(alien)
print(alien)

print("==================显示前三个=================")
# 显示前三个
for k in aliens[:3]:
    print(k)

print("=================显示所有==================")
# 显示所有
for k in aliens:
    print(k)

# 显示列表长度
print(len(aliens))

# 列表中存储字典
pizza = {
    'crust' : 'thick',
    'toppings' : ['mushrooms','extra cheese']
}
print("=================列表中存储字典=================")
for topping in pizza['toppings']:
    print(topping)

# 字典里面存储列表
favorite_languages = {
    'song' : ['java','c'],
    'lv' : ['java','c++'],
    'jun': ['javascript', 'c++'],
}
print("=================字典里面存储列表=================")
for name, language in favorite_languages.items():
    print(name + " love course are: ")
    for lang in language:
        print(lang)


# 字典里面存储字典
print("=================字典里面存储字典=================")
user = {
    'info' : {
        'age' : 20,
        'password' : '123456',
        'address' : '上海',
    },
    'love' : {
        'age': 30,
        'password': '456',
        'address': '上海',
    }
}

for info, love in user.items():
    print(user[info]['age'])


for info, love in user.items():
    print(love['age'])
